/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Server.h
 * Author: kasprus
 *
 * Created on 13 maja 2017, 02:38
 */

#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include <chrono>

using boost::asio::ip::tcp;

class Server{
private:
    boost::asio::io_service io_service;
    tcp::acceptor acceptor;
    
public:
    Server():io_service(), acceptor(io_service, tcp::endpoint(tcp::v4(), 1333)){
    }
    std::string receiveAndSend(std::string message){
      //  try{
            std::string receivedMessege;
            tcp::socket socket(io_service);
            acceptor.accept(socket);
            boost::system::error_code ignored_error;
            boost::array<char, 128> tmpBuffer;
            tmpBuffer.fill(0);
            std::copy(message.begin(), message.end(), tmpBuffer.begin());
            int len=boost::asio::read(socket, boost::asio::buffer(tmpBuffer, tmpBuffer.size()), ignored_error);
            boost::asio::write(socket, boost::asio::buffer(message), ignored_error);
            std::string dupa;
            std::cout<<len<<std::endl;
            for(auto i=tmpBuffer.begin(); len>0; ++i, --len){
                std::cout<<*i<<std::endl;
                dupa+= *i;
            }
      //  }
       // return "";
            return dupa;
    }
    //void send()
    
    
};


#endif /* SERVER_H */

